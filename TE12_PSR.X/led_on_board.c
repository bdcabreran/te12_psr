#include <stdio.h>
#include <stdlib.h>
#include <plib.h>
#include "led_on_board.h"
#include "titoma_timers.h"


LED_struct st_led_green;
LED_struct st_led_yellow;

void f_led_init(u8 u8_color){
    
    if (u8_color == 'g') mPORTGSetPinsDigitalOut(BIT_6); //RD1 -> Output -> LED1 (green)
    else if (u8_color == 'y') mPORTDSetPinsDigitalOut(BIT_1); //RD1 -> Output -> LED2 (yellow)
    mPORTGClearBits(BIT_6);
    mPORTDClearBits(BIT_1);
    
    st_led_green.Macro_on_time = 200;
    st_led_green.Macro_off_time = 800;
    st_led_green.Micro_off_time = 0;
    st_led_green.Micro_off_time = 10;
    st_led_green.Port = 'G';
    st_led_green.Pin = 6;
    st_led_green.Status = 0;

    st_led_yellow.Macro_on_time = 200;
    st_led_yellow.Macro_off_time = 800;
    st_led_yellow.Micro_off_time = 0;
    st_led_yellow.Micro_off_time = 10;
    st_led_yellow.Port = 'D';
    st_led_yellow.Pin = 1;
    st_led_yellow.Status = 0;
}

void f_led_on(u8 u8_color){
    if (u8_color == 'g') mPORTGSetBits(BIT_6); //RG6 -> HIGH -> LED1 on
    else if (u8_color == 'y') mPORTDSetBits(BIT_1); //RD1 -> HIGH -> LED2 on   
/*
 * Example
 * f_led_on('g');
 */
}
void f_led_off(u8 u8_color){
    if (u8_color == 'g') mPORTGClearBits(BIT_6); //RG6 -> HIGH -> LED1 on
    else if (u8_color == 'y') mPORTDClearBits(BIT_1); //RD1 -> HIGH -> LED2 on
/*
 * Example
 * f_led_off('g');
 */    
}

void f_led_brightness(u8 u8_color, vu8 *vu8_led_millis,u8 t_on,u8 t_off){
    if((*vu8_led_millis < t_on))
        f_led_on(u8_color);
    else if(*vu8_led_millis < (t_on + t_off))
        f_led_off(u8_color);
    else
    *vu8_led_millis = 0;
/*
 * Example
 * f_led_brightness('g', &vu8_led_ms , 1, 19); sets 5% brightness in 20ms period
 */    
}

void f_led_blink(u8 u8_color, vu8 *vu8_led_millis, vu8 T_on,vu8 T_off){
    if((*vu8_led_millis < T_on))
        f_led_on(u8_color);
    else if(*vu8_led_millis < (T_on + T_off))
        f_led_off(u8_color);
    else
    *vu8_led_millis = 0;
/*
 * Example
 * f_led_blink('g', &vu8_led_ms , 200, 800); sets 200ms on blink in 1s period
 */
}

void f_led_set_parameters(u8 u8_color, u16 T_on, u16 T_off, u8 t_on, u8 t_off){
    switch (u8_color){
        case 'g':
            st_led_green.Macro_on_time = T_on;
            st_led_green.Macro_off_time = T_off;
            st_led_green.Micro_off_time = t_on;
            st_led_green.Micro_off_time = t_off;
            break;
        case 'y':
            st_led_yellow.Macro_on_time = T_on;
            st_led_yellow.Macro_off_time = T_off;
            st_led_yellow.Micro_off_time = t_on;
            st_led_yellow.Micro_off_time = t_off;
                break;
    }  
}