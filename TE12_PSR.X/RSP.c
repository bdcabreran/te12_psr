/* 
 * File:   RSP.c
 * Author: nb3
 *
 * Created on July 10, 2017, 4:44 PM
 */

/* 	Row --> Player 1
	Column --> Player 2
		R	P	S
	R	2	0	1
	P	1	2	0
	S	0	1	2
*/

#include <stdio.h>
#include <stdlib.h>
#include "datatypes.h"
#include "RSP.h"




u8 table_1[3][3] = {	{0, 2, 1},
						{1, 0, 2},
						{2, 1, 0}};

u8 f_converter(u8 u8_value)
{
 if(u8_value == 'R')return 0; 
 if(u8_value == 'P')return 1;
 if(u8_value == 'S')return 2;
 if(u8_value == 0)return 'R'; 
 if(u8_value == 1)return 'P';
 if(u8_value == 2)return 'S';
}

#ifdef Referee

u8 Result(u8 P1, u8 P2){
	return table_1[f_converter(P1)][f_converter(P2)];
}
#endif



// **********************************************************
#ifdef Player
u8 move(u8 last_move, u8 who_win){
	u8 move = f_converter(last_move);
    u8 randm = rand()%240;
	if(who_win){
		if(randm <= 100)
			return f_converter(move % 3);
		else{
			if(100 < randm && randm < 170)
				return f_converter((move + 1) % 3);
			else
				return f_converter((move + 2) % 3);
		}
	}
    else
    {
    		if(randm < 100)
			return f_converter((move + 1) % 3);
            else{
			if(100 < randm && randm < 170)
				return f_converter((move) % 3);
			else
				return f_converter((move + 2) % 3);
		}
    }
}
#endif


