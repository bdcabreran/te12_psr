#ifndef _TITOMA_PLAYER_H
#define _TITOMA_PLAYER_H

#include "datatypes.h"
#include "game_commands.h"
/*******************struct variables******************/

typedef struct{
 	u8   u8_counter_movement;
 	u8   u8_results[11];
 	u16  u16_round_winner;
 	u16  u16_game_winner;
 	u8   u8_rounds_limit;
    u8   u8_ready_to_play;
 	u16  u16_id;
}st_players;

/*******************struct variables declaration******************/

st_players st_player;


void f_send_id(void);
void f_play(void);
void f_i_m_ready(void);
void  f_players_init(void);
void f_commands(void);





#endif
