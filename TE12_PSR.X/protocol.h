/* 
 * File:   protocol.h
 * Author: nb2
 *
 * Created on July 10, 2017, 12:37 PM
 */

#ifndef PROTOCOL_H
#define	PROTOCOL_H

#ifdef	__cplusplus
extern "C" {
#endif
    
#define buff_size 64 
    
//PROTOCOL FUNCTIONS    
#define GLTON     'A'
#define GLTOFF    'B'
#define GLtON     'C'
#define GLtOFF    'D'
#define GLINFO    'E' 
#define GL_EN_DIS 'F' 
#define YLTON     'G'
#define YLTOFF    'H'
#define YLtON     'I'
#define YLtOFF    'J'
#define YLINFO    'K' 
#define YL_EN_DIS 'L'        
#define GADC      'M'
#define RADC      'N'
#define MESS      'O'
    
#define init_str  '#' 
#define final_str '*'      
  
#include "datatypes.h" 
#include "plib.h"
#include "pic32_uart.h"
#include "string.h"

struct struct_protocol{
    unsigned char source [3];
    unsigned char destination [3];
    unsigned char command[2];
    unsigned char data_str[buff_size];
   
    vu16 v16_data;
    unsigned char UART_id;
};
    
extern struct struct_protocol UART_protocol; 

void f_init_UARTS();
void f_protocol_functions(unsigned char data_buffer[buff_size], u8 UART_id);
vu16 f_str_to_int(const char *str);
void print_source(void);
void f_functions(void);
void f_send_pack(ci16 ci16_id, ci8 ci8_command, u8 ci32_value[buff_size], u8 UART_id);
char* f_get_player_id(void);
char* f_get_command_id(void);
char* f_get_value(void);

//LEDs



#ifdef	__cplusplus
}
#endif

#endif	/* PROTOCOL_H */


