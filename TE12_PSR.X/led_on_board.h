/* 
 * File:   led_on_board.h
 * Author: nb2
 *
 * Created on July 10, 2017, 12:37 PM
 */

#ifndef LED_ON_BOARD_H
#define	LED_ON_BOARD_H

#include "datatypes.h"

#ifdef	__cplusplus
extern "C" {
#endif

    
    typedef struct {
    u16 Macro_on_time;
    u16 Macro_off_time;
    u8 Micro_on_time;
    u8 Micro_off_time;
    u8 Status;
    u8 Pin;
    u8 Port;
    }LED_struct;
    void f_led_on(u8 u8_color);
    void f_led_off(u8 u8_color);
    void f_led_blink(u8 u8_color, vu8 *vu8_led_millis, vu8 T_on,vu8 T_off);


#ifdef	__cplusplus
}
#endif

#endif	/* LED_ON_BOARD_H */

