#include "titoma_game.h"
#include "titoma_timers.h"
#include "protocol.h"
#include "plib.h"
#include <stdio.h>

//get id for player
void get_id(void)
{
 st_player[0].u16_id=0xAA;
 //*st_player_temp.u16_id=f_board_id(); //GET RANDOM ID FROM TIMERS

void f_play(void)
{
  f_send_pack(0xffff,G_PLAY,0,'1');//SENDS MOVES TO THE REFEREE
}

void f_round_results()
{
 f_round_results();
 //format the results-> st_referee_one.u8_results[1][st_referee_one.u8_counter_movement]
 f_send_pack(0xffff,G_RESULTS,'2','1');//SENDS Results 0,1,2

}
void f_game_play()
{
      /***********************************/
      if((st_referee_one.u8_ready_to_play[0])&&(st_referee_one.u8_ready_to_play[1]))
      {
           f_play();

       if(st_referee_one.u8_counter_movement<10)
       {
         st_referee_one.u8_counter_movement++;
         f_round_results();//not implemented yet

       }
       else
       {
         f_final_results();//not implemented yet
         st_referee_one.u8_counter_movement=0;
       }
    }
      //only at round 11th  final result should be send it
}

void f_referee_init()
{
  st_referee_one.u8_counter_movement=0;
  //st_referee_one.u8_results[2][11];
  st_referee_one.u16_round_winner='0';
  st_referee_one.u16_game_winner='0';
  st_referee_one.u16_round_time=2000;
  st_referee_one.u8_rounds_limit=11;
  st_referee_one.u16_player_id[0]=0;
  st_referee_one.u16_player_id[1]=0;
}


void f_game_reset()
{
 f_send_pack(0xffff, G_RESET,0,G_UART_ID);//SEND RESET TO BOTH PLAYERS
 f_referee_init();
 f_send_pack(0xffff,G_WHO_ARE_YOU,G_UART_ID);//ask to the boards for the id
}



void f_round_results()
{
 u8_round_result=result(st_referee_one.u8_current_move[0],st_referee_one.u8_current_move[1]);
 st_referee_one.u8_results[st_referee_one.u8_counter_movement]=u8_round_result;
}


void f_final_results();

void f_playing()
{
   if(!RESET_GAME_BUTTON)
   {
    f_game_reset();
    f_game_play();
   }else
   {
     if(f_round_millis()>2000)
     {
      f_clear_round_millis();
      f_game_play():
     }
   }


}

//void f_send_pack(ci16 ci16_id, ci8 ci8_command, u8 ci32_value[buff_size], u8 UART_id);

void f_commands()
{
  switch(f_get_command_id())
  {
   case G_ID:   st_referee_one.u16_player_id[u8_uart_scr]=f_str_to_int(f_get_value()); break;//change to u16_..[uart id]<-id should be 1-0
   case G_MOVES_ONE: st_referee_one.u8_current_move[u8_uart_scr]=f_str_to_int(f_get_value());break;
   case G_READY    : st_referee_one.u8_ready_to_play[u8_uart_scr]=f_str_to_int(f_get_value());break;
  }

}
