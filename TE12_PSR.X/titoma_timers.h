/* 
 * File:   titoma_timers.h
 * Author: nb2
 *
 * Created on July 10, 2017, 12:38 PM
 */

#ifndef TITOMA_TIMERS_H
#define	TITOMA_TIMERS_H

#ifdef	__cplusplus
extern "C" {
#endif

    #ifndef SYS_FREQ
        #define SYS_FREQ			(96000000UL)
    #endif

    #ifndef CORE_TICK_RATE
        #define CORE_TICK_RATE (SYS_FREQ / 2 / 1000)
    #endif

    
    vu16 vu16_led_green_millis = 0;
    vu16 vu16_led_yellow_millis = 0;
    vu16 vu16_ID_millis = 0;
    vu16 vu16_round_millis = 0;

    void f_core_timer_init(void);
    vu16 f_board_id(void);
    
    
#ifdef	__cplusplus
}
#endif

#endif	/* TITOMA_TIMERS_H */

