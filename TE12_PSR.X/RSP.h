/* 
 * File:   RSP.h
 * Author: nb3
 *
 * Created on July 10, 2017, 4:44 PM
 */

#ifndef RSP_H
#define	RSP_H

#define Referee
#define Player

#ifdef	__cplusplus
extern "C" {
#endif

  
u8 f_converter(u8 u8_value);
#ifdef  Referee
u8 Result(u8 P1, u8 P2);
#endif


#ifdef Player
u8 move(u8 last_move, u8 who_win);
#endif



#ifdef	__cplusplus
}
#endif

#endif	/* RSP_H */

