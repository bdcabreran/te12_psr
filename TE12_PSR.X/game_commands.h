#ifndef _GAME_COMMANDS_H
#define _GAME_COMMANDS_H

#define ROUND_LIMIT 11

#define G_UART_ID     '1'
/*************COMMANDS*********************************/
#define G_RESET       'A'
#define G_PLAY        'B'
#define G_RESULTS     'C'
#define G_FINAL_R     'D'
#define G_WHO_ARE_U   'E'

/*************COMMANDS*********************************/

#define G_ID          'F'
#define G_MOVES       'G'
#define G_READY       'H'

#endif