#ifndef _TITOMA_GAME_H
#define _TITOMA_GAME_H

#include "datatypes.h"
#include "game_commands.h"
#define RESET_GAME mPORTDReadBits(BIT_4)
/*******************struct variables******************/

u8 u8_uart_scr=0;//uart source , uart message arrives at uart1 or uart 2, value 0-1

typedef struct{
 	u8   u8_counter_movement;
	u8   u8_rounds_limit;
 	u8   u8_ready_to_play[2];
    u8   u8_current_move[2];
    u8   u8_moves_log[2][11];
 	u8   u8_results[11];
  /*{{0,1,0,1,0,1,1,0,1,0,1}  player one
    {0,1,0,1,0,1,1,0,1,0,1}}player two*/ 

 	u16  u16_round_winner;
 	u16  u16_game_winner;
 	u16  u16_round_time;
 	u16  u16_player_id[2];
}st_referee;
/*******************struct variables declaration******************/
st_referee st_referee_one;
st_players st_player;



//*************functions, only for referee*******/
void f_play(void);
void f_game_play(void);
void f_game_reset(st_players *);

void f_round_results(void);
void f_final_results(void);
void f_commands();




#endif
