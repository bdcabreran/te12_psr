/* 
 * File:   main.h
 * Author: nb2
 *
 * Created on July 10, 2017, 12:15 PM
 */

#ifndef MAIN_H
#define	MAIN_H

#ifdef	__cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdlib.h>
#include "pic32_uart.h"
#include "protocol.h"    
    
#ifdef	__cplusplus
}
#endif

#endif	/* MAIN_H */