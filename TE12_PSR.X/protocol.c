/* 
 * File:   protocol.c
 * Author: nb2
 *
 * Created on July 10, 2017, 12:37 PM
 */

#include "protocol.h"


struct struct_protocol UART_protocol;
unsigned char i, j;
unsigned char buf_response[buff_size];

#define DEVICE "B2"

#define debug

void f_init_UARTS(){
    
    mPORTDSetPinsDigitalOut(BIT_3); //UART_TX as an output
    mPORTDSetPinsDigitalIn(BIT_2); //UART_RX as an output
    UART1_init(115200);
    UART2_init(115200);
    INTConfigureSystem(INT_SYSTEM_CONFIG_MULT_VECTOR);
    INTEnableInterrupts();
   
}



void f_protocol_functions(unsigned char data_buffer[], u8 UART_id){
    
     UART_protocol.UART_id = UART_id; 
     destination_check(data_buffer);
    
}

void f_extract_info(unsigned char data_buffer[]){
    
    //clear value buffer
     memset(UART_protocol.data_str, 0, sizeof(UART_protocol.data_str));
     
    //source
     UART_protocol.source[0]= data_buffer[0];
     UART_protocol.source[1]= data_buffer[1];
      
    //destination
     UART_protocol.destination[0]= data_buffer[2];
     UART_protocol.destination[1]= data_buffer[3]; 

     
    //command 
     UART_protocol.command[0] = data_buffer[4];


    
    //Value 
    i = 5, j =0;
    while(data_buffer[i] != final_str){
        if(data_buffer[i] != init_str){
            UART_protocol.data_str[j] = data_buffer[i];
            j ++;
        }
        i++;
    }
    UART_protocol.v16_data = f_str_to_int(UART_protocol.data_str);

}

vu16 f_str_to_int(const char *str)
{
   vu16 my_num=0;
   while(*str != '\0')
   {
       my_num = my_num*10 +((u8)(*str) - 0x30);
       str++;
   }
   return my_num;
}

void print_source(void){
    sprintf(U1TxBuf, "source... %s\r\n",UART_protocol.source);
    UART1_write_string(U1TxBuf);
    
    sprintf(U1TxBuf, "destination... %s\r\n",UART_protocol.destination);
    UART1_write_string(U1TxBuf);
    
    sprintf(U1TxBuf, "command... %s\r\n",UART_protocol.command);
    UART1_write_string(U1TxBuf);
    
    sprintf(U1TxBuf, "id... %d\r\n",UART_protocol.UART_id);
    UART1_write_string(U1TxBuf);
    
    sprintf(U1TxBuf, "value... %d\r\n",UART_protocol.v16_data);
    UART1_write_string(U1TxBuf);

}

void destination_check(unsigned char data_buffer[]){
     //destination
     //Echo, this segment return the same string to the source if the source and receiver are the same in the command
     if(!memcmp(&data_buffer[0], &data_buffer[2],2)){
         if(UART_protocol.UART_id == 1){
             UART1_write_string(data_buffer);
         }else{
             UART2_write_string(data_buffer);
         } 
     }else{
         if(!memcmp(DEVICE,&data_buffer[2],2))
         {
        
         //if(DEVICE [0] ==data_buffer[2] && DEVICE [1] == data_buffer[3]){
            f_extract_info(data_buffer);
               #ifdef debug
                   print_source();
               #endif
           f_functions();

        } 
        else{
            if(UART_protocol.UART_id == 1){
                UART2_write_string(data_buffer);
            }else{
                UART1_write_string(data_buffer);
            }    
        }
     }
    
    //if(!strcmp(DEVICE,))

}

void f_functions(void){
    switch (UART_protocol.command[0]){
        case 'A':
            
            
            break;
        case 'B':
            
            
            break;
        case 'C':
            
            break;
        case 'D':
            
            break;         
        case 'E':
            
            break;
        case 'F':
            
            break;          
        case 'G':
            
            break;
        case 'H':
            
            break;          
        case 'I':
            
            break;
        case 'J':
            
            break;          
        case 'K':
            
            break;
        case 'L':
            
            break;          
        case 'M':
            
            break;
        case 'N':
            
            break;          
        case 'O':
            
            break;
    
    }
        
}





//************titoma game************

void f_send_pack(ci16 ci16_id, ci8 ci8_command, u8 ci32_value[buff_size], u8 UART_id)
{    
     buf_response[0] = '?';
     buf_response[1] = '?';
        
     sprintf(&buf_response[2],"%x%c#%s*\r",ci16_id,ci8_command,ci32_value);
     if(UART_id == '1')
        UART1_write_string(buf_response);
     if(UART_id == '2')
        UART2_write_string(buf_response);
       
}

char* f_get_player_id(void){
    //Return a array of 3 char like "/0B1"
    return UART_protocol.destination;
}

char* f_get_command_id(void){
    //Return a array of 2 char like "/0M"
    return UART_protocol.command;
}

char* f_get_value(void){ 
    //Return a array of 64 char like "/0000.....000HOLA"
    return UART_protocol.data_str;
}



//******************Example titoma gme functions*************************

/*
int main(int argc, char** argv) {
    
    
    f_init_UARTS();
    
    sprintf(U1TxBuf, "PIC Initializing....\r\n");
    UART1_write_string(U1TxBuf);
    
    while(1){
        

    if(U2Unread){
        
        //Extract package info
        f_extract_info(U2RxBuf);
        UART2_clear_unread();
        
        //Send a package
        f_send_pack(0xff, 'r', "hola",'2');
        UART2_clear_unread();
        
        //Get package info
        sprintf(U2TxBuf,"Player id is :.... %s\r\n", f_get_player_id());
        UART2_write_string(U2TxBuf);
        sprintf(U2TxBuf,"Command id is :.... %s\r\n", f_get_command_id());
        UART2_write_string(U2TxBuf);
        sprintf(U2TxBuf,"message id is :.... %s\r\n", f_get_value());
        UART2_write_string(U2TxBuf);
        }
    }

    return (EXIT_SUCCESS);
}
*/




