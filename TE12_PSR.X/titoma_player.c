#include "titoma_game.h"
#include "titoma_timers.h"
#include "protocol.h"
#include "plib.h"
#include <stdio.h>
#include "RSP.h"

void f_send_id(void)
{
 f_send_pack(0xffff,G_ID_ONE,f_board_id(),G_UART_ID);//PLAYER SENDS ID TO THE REFEREE
}

void f_play(void)
{
 f_send_pack(0xffff,G_MOVES,move(0,0),'1');//SENDS MOVES TO THE REFEREE
}


void f_i_m_ready(void)
{
  f_send_pack(0xffff,G_READY,0,'1');//SENDS OK TO THE REFEREE
}

void  f_players_init()
{
 st_player.u8_counter_movement=0;
 st_player.u8_rounds_limit=ROUND_LIMIT;
 st_player.u16_id=f_board_id();
 f_i_m_ready();
}

//void f_send_pack(ci16 ci16_id, ci8 ci8_command, u8 ci32_value[buff_size], u8 UART_id);

void f_commands(void)
{
  switch(f_get_command_id())
  {
   case G_RESET: f_players_init();break; //only for players, referee recieves a signal reset at portD
   case G_PLAY: f_play(); break;
   case G_RESULTS: /*STORES THE RESULTS IN THE STRUCTURE*/break;
   case G_FINAL_R: /*STORE THE FINAL RESULT IN THE STRUCTURE USING GET_VALUE*/break;
   case G_WHO_ARE_YOU: f_send_id();break;
   default:break;
  }

}
