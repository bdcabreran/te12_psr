#include <stdio.h>
#include <stdlib.h>
#include "datatypes.h"
#include <plib.h>
#include "titoma_timers.h"

#define CORE_TICK_RATE (SYS_FREQ / 2 / 1000)
#define buttton mPORT





void f_core_timer_init(void){
    OpenCoreTimer(CORE_TICK_RATE);// enable CoreTimer
    mConfigIntCoreTimer((CT_INT_ON | CT_INT_PRIOR_2 | CT_INT_SUB_PRIOR_0));
}
//-------------------------CORE TIMER INTERRUPT FUNCTION------------------------
void __ISR(_CORE_TIMER_VECTOR, IPL2)  CoreTimerHandler(void){
    mCTClearIntFlag();
    UpdateCoreTimer(CORE_TICK_RATE);
    vu16_led_green_millis++;
    vu16_led_yellow_millis++;
    vu16_ID_millis++;
    vu16_round_millis++;
    
}
//----------------------------------FUNCTIONS-----------------------------------
vu16 f_board_id(void){
    return vu16_ID_millis;
}

vu16 f_round_milllis(void){
    return vu16_round_millis;
}
void f_clear_round_millis(void){
    vu16_round_millis = 0;
}